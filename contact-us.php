<?php 

$msg = '';

if(isset($_REQUEST['msg'])) {

	if($_REQUEST['msg'] == 1) {

		$msg = "Your mail has been successfully.";

	} elseif($_REQUEST['msg'] == 301) {

		$msg = "Entered captcha not valid.";

	} else {

		$msg = "Mail  is not send.";

	}

}

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>White7 Shrewsbury </title>
<link href="font-awesome/css/font-awesome.css" type="text/css" rel="stylesheet"  />
<link href="font-awesome/css/font-awesome.min.css" type="text/css" rel="stylesheet"  />
<link href="bootstrap/css/bootstrap.css" type="text/css" rel="stylesheet"  />
<link href='https://fonts.googleapis.com/css?family=Raleway:400,500,700' rel='stylesheet' type='text/css'>
<link rel="shortcut icon" href="favicon.ico" type="image/x-icon">




<meta name="viewport" content="width=device-width, initial-scale=1">
<script type="text/javascript" src="js/common.js"></script>
<script type="text/javascript">

$(document).ready(function() {

	$("#button").click(function() {
	

		var emailReg = /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/;
		
 if($("#name").val() == '') {

			$("#msg").html('Please enter  Name.');

			$("#name").focus();

			return false;
			
	} else if($("#email").val() == '') {

			$("#msg").html('Please enter Email.');

			$("#email").focus();

			return false;

	} else if(!emailReg.test($("#email").val())) {

			$("#msg").html('Please enter valid email.');

			$("#email").focus();

			return false;	
			
			
	} else if($("#telephone").val() == '') {

			$("#msg").html('Please enter  Contact Number');

			$("#telephone").focus();

			return false;

	}	else if($("#services").val() == '0') {

			$("#msg").html('Please Select Services.');

			$("#services").focus();

			return false;
			
	}	else if($("#collection").val() == '') {

			$("#msg").html('Please Enter Collection Address Details');

			$("#collection").focus();

			return false;
			
	}	else if($("#destination").val() == '') {

			$("#msg").html('Please Enter Destination Address ');

			$("#destination").focus();

			return false;
	}	else if($("#returncollection").val() == '') {

			$("#msg").html('Please Enter Return Journey Collection Address');

			$("#returncollection").focus();

			return false;
	}	else if($("#returncollection").val() == '') {

			$("#msg").html('Please Enter  Return Journey Destination Address ');

			$("#returncollection").focus();

			return false;
	
	} else if($("#security_code").val() == '') {

			$("#msg").html('Please Enter 5 Digit code');

			$("#security_code").focus();

			return false;
			
			
			

		} else {

			return true;	

		}

			

	});

	

	$("#cancel").click(function() {

								$("#services").val('');

								$("#name").val('');

								$("#email").val('');

								$("#cname").val('');

								$("#phone").val('');

								$("#inquiry").val('');

								$("#msg").html('');

							

								});

});

</script>


</head>

<body>
<?php include("header.php"); ?>


<div class="dark-gray">
<div class="container innerpage">
<div class="whitebg">




<div class="col-lg-5">

<h1>Contact Us</h1>

<strong>Enquiries and Reservations</strong><br />
T: 01743 455924
<br />
<br />

<strong>White 7 Shrewsbury</strong>

<p>E: david@white7.co.uk<br />

M:  07538 326996<br />

Web : www.white7shrewsbury.co.uk<br />




</p>




</div>

<div class="col-lg-7">
<h1>Enquire </h1>


<form method="post" id="frm" name="frm" action="adminmail.php" >

 <input type="hidden" name="posted" value="1">
<div class="callbackform">
<div class="calbackRow">
<label>Name<span> *</span></label>
<input name="name" id="name" type="text" />
</div>

<div class="calbackRow">
<label>Your Email <span>*</span></label>
<input  type="text" name="email" id="email" /> </div>

<div class="calbackRow">

<label>
Contact Telephone Number <span>*</span></label>

<input name="telephone" id="telephone" type="text" />
</div>

<div class="calbackRow">
<label>Service Interested in <span>*</span></label>


 <select name="services" id="services" class="text_boxin2">



  <option value="0">-----</option>
<option value="Weddings">Weddings</option><option value="Corporate">Corporate</option><option value="Experiences">Experiences</option><option value="Franchising">Franchising</option></select>


</select> </div>

<!--<div class="calbackRow">
<label>Date service (required)<span>*</span></label> <b>Format : dd/mm/yy</b>



<input name="dateservices" id="dateservices"  type="text" /></div>-->

<div class="calbackRow">
<label>Collection Address Details  <span>*</span> </label>  <b> format: dd/mm/yy and time</b>

<input name="collection" id="collection"  type="text" /></div>


<div class="calbackRow">
<label> Destination Address Details <span>*</span></label>  <b> format: dd/mm/yy and time</b>

<input name="destination" id="destination"  type="text" /></div>



<div class="calbackRow">
<label> Return Journey Collection Address Details <span>*</span> </label> <b> format: dd/mm/yy and time</b>

<input name="returncollection" id="returncollection"  type="text" /></div>


<div class="calbackRow">
<label> Return Journey Destination Address Details <span>*</span> </label> <b> format: dd/mm/yy and time</b>

<input name="redestination" id="redestination"  type="text" /></div>





<div class="calbackRow">
<label>How did you find us</label><br />
 <select name="findus" id="findus" class="text_boxin2">

  <option value="0">-----</option>


<option value="Search Engine">Search Engine</option><option value="Press">Press</option><option value="Local Advert">Local Advert</option><option value="Wedding Fayre">Wedding Fayre</option></select>


</div>



<div class="calbackRow">


<label>Security Code</label><br />


<img src="CaptchaSecurityImages.php?width=100&height=40&characters=5" alt="" style="float:left; margin-right:10px">

		<input id="security_code" name="security_code" type="text" style="width:50%;" placeholder="Enter 5 Digit code" >

</div>
<div class="calbackRow"> 
 <input type="submit" name="button" id="button" value="Submit" class="subtm">


<span id="msg" style="color:#FF0000; font-weight:bold; font-size:11px; font-family:Tahoma;"><?php echo $msg;?></span>



 </div>


<span style="color:#FF0000; font-size:25px; line-height:25px;">* </span>All field mandatory
</div>

</form>



</div>

<div class="cl"></div>

</div>




<?php include("footer.php"); ?>




</div></div>

<script src="responsive-tab/jquery.1.10.2.js" type="text/javascript"></script>

<link type="text/css" rel="stylesheet" href="responsive-tab/easy-responsive-tabs.css" />
<script src="responsive-tab/easyResponsiveTabs.js" type="text/javascript"></script>

<script type="text/javascript">
    $(document).ready(function () {
        $('#horizontalTab').easyResponsiveTabs({
            type: 'default', //Types: default, vertical, accordion           
            width: 'auto', //auto or any width like 600px
            fit: true,   // 100% fit in a container
            closed: 'accordion', // Start closed if in accordion view
            activate: function(event) { // Callback function if tab is switched
                var $tab = $(this);
                var $info = $('#tabInfo');
                var $name = $('span', $info);

                $name.text($tab.text());

                $info.show();
            }
        });

        $('#verticalTab').easyResponsiveTabs({
            type: 'vertical',
            width: 'auto',
            fit: true
        });
    });
</script>


<script>
$(document).ready(function() {

	$('#menu-toggle').click(function () {
      $('#menu').toggleClass('open');
      e.preventDefault();
    });
    
});
</script>


</body>
</html>
