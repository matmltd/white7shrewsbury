<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>White7 Shrewsbury </title>
<link href="font-awesome/css/font-awesome.css" type="text/css" rel="stylesheet"  />
<link href="font-awesome/css/font-awesome.min.css" type="text/css" rel="stylesheet"  />
<link href="bootstrap/css/bootstrap.css" type="text/css" rel="stylesheet"  />
<link href='https://fonts.googleapis.com/css?family=Raleway:400,500,700' rel='stylesheet' type='text/css'>
<link rel="shortcut icon" href="favicon.ico" type="image/x-icon">
<script type="text/javascript" src="js/jquery-1.3.2.min.js"></script>
<script type="text/javascript" src="js/jquery.cycle.all.min.js"></script>

<script type="text/javascript">
$(function() {


$('#slideshow').cycle({ 
    fx:     'fade', 
    speed:  800, 
    timeout: 4000, 
    next:   '#next2', 
    prev:   '#prev2' 
});

  
});



</script>

<meta name="viewport" content="width=device-width, initial-scale=1">
</head>

<body>
<?php include("header.php"); ?>


<div class="dark-gray">
<div class="container innerpage">
<div class="whitebg">

<h1>Corporate</h1>


<div class="col-lg-7"><p>White 7 Shrewsbury has been designed to accommodate business travel. Our motto is comfort, convenience and confidentiality which is the life blood to create longevity between us and the client.</p>

<p>As the door is opened for you and you sit inside one of our BMW fleet you can relax as the journey begins. For your comfort and safety, the BMW 730 M Sport is designed for the business executive to travel in comfort and style and when coupled with a professionally qualified chauffeur  you will be prepared to work on the move.</p>

<p>Convenience and confidentiality allows the client and /  or their guests to be taken from ‘ door to door ‘ and without the stress, prescribed timetables and lack of privacy associated with public transport. With on board drinks to keep you refreshed, you will arrive at your destination suitably prepared and ready to do business.</p>

<p>White 7 can be used as part of your marketing budget and can pick up clients from Heathrow, Gatwick, Manchester and Birmingham Airports to ensure their safe travel directly to your premises. Also, your executives can use this service for internal staff motivation and reward.</p>

<a href="contact-us.php" class="engBtn">Enquire</a>
</div>

<div class="col-lg-5">
<img src="images/Corporate1.jpg" class="imglisting" />

<img src="images/Corporate2.jpg" class="imglisting" />




</div>
<div class="cl"></div>
<p align="center">
<img src="images/Corporate3.jpg" /> </p>

<div class="cl"></div>

</div>

<?php include("footer.php"); ?>




</div>

</div>


<script src="responsive-tab/jquery.1.10.2.js" type="text/javascript"></script>

<link type="text/css" rel="stylesheet" href="responsive-tab/easy-responsive-tabs.css" />
<script src="responsive-tab/easyResponsiveTabs.js" type="text/javascript"></script>

<script type="text/javascript">
    $(document).ready(function () {
        $('#horizontalTab').easyResponsiveTabs({
            type: 'default', //Types: default, vertical, accordion           
            width: 'auto', //auto or any width like 600px
            fit: true,   // 100% fit in a container
            closed: 'accordion', // Start closed if in accordion view
            activate: function(event) { // Callback function if tab is switched
                var $tab = $(this);
                var $info = $('#tabInfo');
                var $name = $('span', $info);

                $name.text($tab.text());

                $info.show();
            }
        });

        $('#verticalTab').easyResponsiveTabs({
            type: 'vertical',
            width: 'auto',
            fit: true
        });
    });
</script>


<script>
$(document).ready(function() {

	$('#menu-toggle').click(function () {
      $('#menu').toggleClass('open');
      e.preventDefault();
    });
    
});
</script>

</body>
</html>
