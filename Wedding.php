<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>White7 Shrewsbury </title>
<link href="font-awesome/css/font-awesome.css" type="text/css" rel="stylesheet"  />
<link href="font-awesome/css/font-awesome.min.css" type="text/css" rel="stylesheet"  />
<link href="bootstrap/css/bootstrap.css" type="text/css" rel="stylesheet"  />
<link href='https://fonts.googleapis.com/css?family=Raleway:400,500,700' rel='stylesheet' type='text/css'>
<link rel="shortcut icon" href="favicon.ico" type="image/x-icon">
<script type="text/javascript" src="js/jquery-1.3.2.min.js"></script>
<script type="text/javascript" src="js/jquery.cycle.all.min.js"></script>

<script type="text/javascript">
$(function() {


$('#slideshow').cycle({ 
    fx:     'fade', 
    speed:  800, 
    timeout: 4000, 
    next:   '#next2', 
    prev:   '#prev2' 
});

  
});



</script>

<meta name="viewport" content="width=device-width, initial-scale=1">
</head>

<body>
<?php include("header.php"); ?>


<div class="dark-gray">
<div class="container innerpage">
<div class="whitebg">


<h1>Wedding</h1>


<div class="col-lg-7"><p>A wedding day should be a perfect day for the bride, her groom and their families and friends.</p>

<p>We take great pride in providing the very best chauffeured wedding service, thanks to our fleet of gleaming new model luxury BMW cars and our professionally qualified female and male chauffeurs.</p>

<p>Where ever your wedding venue might be, we can take you there in style and comfort, with the certainty that we will get you there on time.</p>

<p><strong class="bigtext" style="color:#3d3b3b;">“If this is your big day let <br />

<div style="text-align:right; display:block;">
White 7 make  it your perfect day.”</div></strong></p>




</div>

<div class="col-lg-5"><img src="images/wedding-img.jpg" alt="Wedding" /></div>
<div class="cl"></div>



<div class="cl"></div>
<div class="lightgray">
<h2>The Ultimate Wedding Experience</h2>
<p>
A Day of Celebration: A Wedding day should be a perfect day for the bride, her groom their families and friends.</p>
<p>

We take great pride in providing the very best chauffeured wedding service, thanks to our fleet of gleaming new model
luxury BMW cars and our professionally qualified female and male chauffeurs. </p>
<p>

Where ever your wedding venue might be, we can take you there in style and comfort, with the certainty that we will get
you there on time. If this is your big day – let White 7 make it your perfect day.</p>


<div class="serviceMain">

<div class="serviceListing">
<div class="col-lg-8">
<h5>The Limousine Service</h5>

<span class="red">£275.00* up to 4 guests.</span>

<p>The limousine service incorporating one chauffeured vehicle: <br />

BMW 730 M Sport </p>
</div>
<div class="col-lg-4">
<div class="arrowmain"></div>
<div class="serviceRight"><span>01</span>Limousine </div>
</div>
<div class="cl"></div>

</div>



<div class="serviceListing">
<div class="col-lg-8">
<h5>The Ribbon Service</h5>

<span class="red">£450.00* up to 8 guests.</span>

<p>The ribbon service incorporating two chauffeured vehicles:  <br />

BMW 730 M Sport  • BMW 5 Series M Sport </p>
</div>
<div class="col-lg-4">
<div class="arrowmain"></div>
<div class="serviceRight"><span>02</span>Ribbon </div>
</div>
<div class="cl"></div>

</div>


<div class="serviceListing">
<div class="col-lg-8">
<h5>The Champagne Service</h5>

<span class="red">£625.00* up to 12 guests.</span>

<p>The champagne service incorporating three chauffeured vehicles:<br />

BMW 730 M Sport  • BMW 5 Series M Sport • BMW 5 GT Series M Sport </p>
</div>
<div class="col-lg-4">
<div class="arrowmain"></div>
<div class="serviceRight"><span>03</span>Champagne </div>
</div>
<div class="cl"></div>

</div>



<div class="serviceListing">
<div class="col-lg-8">
<h5>The Complete Service</h5>

<span class="red">£795.00* up to 16 guests.</span>

<p>The complete service incorporating four chauffeured vehicles: <br />
BMW 730 M Sport

• BMW 5 Series M Sport • BMW 5 GT Series M Sport • BMW 5 Series M Sport </p>
</div>
<div class="col-lg-4">
<div class="arrowmain"></div>
<div class="serviceRight"><span>04</span>Complete </div>
</div>
<div class="cl"></div>

</div>



<div class="serviceListing">
<div class="col-lg-8">
<h5>The Ultimate Service</h5>

<span class="red">£950.00* up to 20 guests.</span>

<p>The ultimate service incorporating five chauffeured vehicles: <br />
BMW 730 M Sport • BMW 5
Series M Sport • BMW 5 GT series M Sport • BMW 5 Series M Sport • BMW 5 Series SE</p>
</div>
<div class="col-lg-4">
<div class="arrowmain"></div>
<div class="serviceRight"><span>05</span>Ultimate </div>
</div>
<div class="cl"></div>

</div>


</div>


<a href="contact-us.php" class="engBtn">Enquire</a>

</div>
</div>

<?php include("footer.php"); ?>




</div>

</div>

<script>
$(document).ready(function() {

	$('#menu-toggle').click(function () {
      $('#menu').toggleClass('open');
      e.preventDefault();
    });
    
});
</script>

</body>
</html>
