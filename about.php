<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>White7 Shrewsbury </title>
<link href="font-awesome/css/font-awesome.css" type="text/css" rel="stylesheet"  />
<link href="font-awesome/css/font-awesome.min.css" type="text/css" rel="stylesheet"  />
<link href="bootstrap/css/bootstrap.css" type="text/css" rel="stylesheet"  />
<link href='https://fonts.googleapis.com/css?family=Raleway:400,500,700' rel='stylesheet' type='text/css'>
<link rel="shortcut icon" href="favicon.ico" type="image/x-icon">
<script type="text/javascript" src="js/jquery-1.3.2.min.js"></script>
<script type="text/javascript" src="js/jquery.cycle.all.min.js"></script>

<script type="text/javascript">
$(function() {


$('#slideshow').cycle({ 
    fx:     'fade', 
    speed:  800, 
    timeout: 4000, 
    next:   '#next2', 
    prev:   '#prev2' 
});

  
});



</script>

<meta name="viewport" content="width=device-width, initial-scale=1">
</head>

<body>
<?php include("header.php"); ?>


<div class="dark-gray">
<div class="container innerpage">
<div class="whitebg">

<h1>About White 7 Shrewsbury</h1>


<div class="col-lg-7">
<p><strong>David</strong> served for 31 years as a Police Officer and for the last 10 years as Chairman of the Police Federation ( Constables ) based in Surrey. On leaving the service, he became Chief Executive Officer of the Police Dependents Trust. He was invited by G4S to assist them in the 2012 Olympic games partly because of his previous extensive Police service. David later worked for White 7 for two years combining his roles between being a Chauffeur and Office Manager. Having gained this valuable experience, he recognised the opportunity of having his own franchise hence his investment into White 7 Shrewsbury.</p>
<p>White 7 founding director Anthony Randall, said: “A great deal of effort has been put into forming and developing this company over a relatively short period of time, with the sole purpose of expanding the brand across the UK having been a priority from the very beginning.</p>
<p>We are delighted that David can see the brand value and future potential of his White 7 franchise and that he recognises the core elements of convenience, confidentiality and quality of service offered by the company.</p>
<p>“Following lengthy consultations, we were very particular in vetting potential candidates to represent us in strategic locations across the UK, so we are absolutely thrilled and excited by this appointment and are confident that it will be a huge success in the south of the country.</p>
<p>White 7 is a major partner in the British Chauffeurs Guild, teaming up to offer
dedicated driver training courses to professional chauffeurs. David has already completed the chauffeur programme and as such is an authorised chauffeur of The British Chauffeurs Guild </p>

</div>

<div class="col-lg-5">
<img src="images/devid.jpg" alt="devid" />



</div>
<div class="cl"></div>


<div class="cl"></div>

</div>

<?php include("footer.php"); ?>




</div>

</div>


<script src="responsive-tab/jquery.1.10.2.js" type="text/javascript"></script>

<link type="text/css" rel="stylesheet" href="responsive-tab/easy-responsive-tabs.css" />
<script src="responsive-tab/easyResponsiveTabs.js" type="text/javascript"></script>

<script type="text/javascript">
    $(document).ready(function () {
        $('#horizontalTab').easyResponsiveTabs({
            type: 'default', //Types: default, vertical, accordion           
            width: 'auto', //auto or any width like 600px
            fit: true,   // 100% fit in a container
            closed: 'accordion', // Start closed if in accordion view
            activate: function(event) { // Callback function if tab is switched
                var $tab = $(this);
                var $info = $('#tabInfo');
                var $name = $('span', $info);

                $name.text($tab.text());

                $info.show();
            }
        });

        $('#verticalTab').easyResponsiveTabs({
            type: 'vertical',
            width: 'auto',
            fit: true
        });
    });
</script>


<script>
$(document).ready(function() {

	$('#menu-toggle').click(function () {
      $('#menu').toggleClass('open');
      e.preventDefault();
    });
    
});
</script>

</body>
</html>
