<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>White7 Shrewsbury </title>
<link href="font-awesome/css/font-awesome.css" type="text/css" rel="stylesheet"  />
<link href="font-awesome/css/font-awesome.min.css" type="text/css" rel="stylesheet"  />
<link href="bootstrap/css/bootstrap.css" type="text/css" rel="stylesheet"  />
<link href='https://fonts.googleapis.com/css?family=Raleway:400,500,700' rel='stylesheet' type='text/css'>

<script type="text/javascript" src="js/jquery-1.3.2.min.js"></script>
<script type="text/javascript" src="js/jquery.cycle.all.min.js"></script>

<script type="text/javascript">
$(function() {


$('#slideshow').cycle({ 
    fx:     'fade', 
    speed:  800, 
    timeout: 4000, 
    next:   '#next2', 
    prev:   '#prev2' 
});

  
});



</script>

<meta name="viewport" content="width=device-width, initial-scale=1">
</head>

<body>
<?php include("header.php"); ?>


<div class="dark-gray">
<div class="container innerpage">
<div class="whitebg">
<div class="sitemap">
<h1>Site Map</h1>

<ul>
<li><a href="index.php">Home   </a></li>
<li><a href="Wedding.php">Wedding   </a></li>
<li><a href="experiences.php">Experiences   </a></li>
<li><a href="corporate.php">Corporate    </a></li>
<li><a href="contact-us.php">Contact Us</a></li>




</ul>
</div>

<div class="cl"></div>

</div>

<?php include("footer.php"); ?>




</div>


<script src="responsive-tab/jquery.1.10.2.js" type="text/javascript"></script>

<link type="text/css" rel="stylesheet" href="responsive-tab/easy-responsive-tabs.css" />
<script src="responsive-tab/easyResponsiveTabs.js" type="text/javascript"></script>

<script type="text/javascript">
    $(document).ready(function () {
        $('#horizontalTab').easyResponsiveTabs({
            type: 'default', //Types: default, vertical, accordion           
            width: 'auto', //auto or any width like 600px
            fit: true,   // 100% fit in a container
            closed: 'accordion', // Start closed if in accordion view
            activate: function(event) { // Callback function if tab is switched
                var $tab = $(this);
                var $info = $('#tabInfo');
                var $name = $('span', $info);

                $name.text($tab.text());

                $info.show();
            }
        });

        $('#verticalTab').easyResponsiveTabs({
            type: 'vertical',
            width: 'auto',
            fit: true
        });
    });
</script>


<script>
$(document).ready(function() {

	$('#menu-toggle').click(function () {
      $('#menu').toggleClass('open');
      e.preventDefault();
    });
    
});
</script>

</body>
</html>
