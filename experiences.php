<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>White7 Shrewsbury </title>
<link href="font-awesome/css/font-awesome.css" type="text/css" rel="stylesheet"  />
<link href="font-awesome/css/font-awesome.min.css" type="text/css" rel="stylesheet"  />
<link href="bootstrap/css/bootstrap.css" type="text/css" rel="stylesheet"  />
<link href='https://fonts.googleapis.com/css?family=Raleway:400,500,700' rel='stylesheet' type='text/css'>
<link rel="shortcut icon" href="favicon.ico" type="image/x-icon">
<script type="text/javascript" src="js/jquery-1.3.2.min.js"></script>
<script type="text/javascript" src="js/jquery.cycle.all.min.js"></script>

<script type="text/javascript">
$(function() {


$('#slideshow').cycle({ 
    fx:     'fade', 
    speed:  800, 
    timeout: 4000, 
    next:   '#next2', 
    prev:   '#prev2' 
});

  
});



</script>

<meta name="viewport" content="width=device-width, initial-scale=1">
</head>

<body>
<?php include("header.php"); ?>


<div class="dark-gray">
<div class="container innerpage">
<div class="whitebg">


<h1>Experiences</h1>


<div class="col-lg-8"><p>Are there times when for that special occasion you are unsure what to buy or arrange.?? It could be a birthday, a wedding anniversary or an engagement. Why not do something different and let White 7 chauffeur you to your chosen venue whether it be fine dining, the theatre or a relaxing  Spa day. Why not extend your celebration with a hotel reservation and let White 7 take you and return you home in style, comfort and safety. Our lead vehicle the BMW 730 M Sport is fitted out with all the required electronic equipment and you can give us a bottle of Wine or champagne to place in the rear cabin chiller to help make it the perfect occasion.</p>


<div id="verticalTab">
        <ul class="resp-tabs-list">
             <li class="TabbedPanelsTab" tabindex="0">Sport <span></span> </li>
             <li class="TabbedPanelsTab" tabindex="0">Airport <span></span>  </li>
             <li class="TabbedPanelsTab" tabindex="0">Shopping <span></span>  </li>
             <li class="TabbedPanelsTab" tabindex="0">Pamper /Spa <span></span>  </li>
             <li class="TabbedPanelsTab" tabindex="0">Fine Dining<span></span>   </li>
             <li class="TabbedPanelsTab" tabindex="0">Theatre <span></span> </li>
             


        </ul>
        
        <div class="resp-tabs-container">
        <div class="TabbedPanelsContent"> 
      <p>There are an abundance of major sporting occasions in the United Kingdom every year, not only in the actual sport itself but also the competitions. This ranges from Football with the Premier League or FA Cup, Rugby with the Home Internationals and in 2015 the World Cup, Cricket at County Level or International Matches, Tennis at Wimbledon, Motor Racing at the British Grand Prix and Rowing at the Henley Regatta to name just a few.</p>
<p>
Whatever your sporting choice may be, let White 7 help make it your perfect sporting Experience.</p>

<a href="contact-us.php" class="engBtn">Enquire</a>
       <div class="cl"></div> 
        </div>
                <div class="TabbedPanelsContent"> <p>Traditionally people have driven to the airport and parked ‘on’ or ‘off’ site for a week or more as part of their holiday destination. There are a number of considerations to this strategy. Stress in driving to the airport ( or Cruise Liner Terminal ), parking and catching the airport bus or even worse the mini bus if ‘off’ site to the terminal building and the hassle of handling the luggage from one vehicle to another. Your holiday should commence from the moment you leave home. Let White 7 take all the inconvenience and hard work away from you by Chauffeuring you in a White 7 limousine where you can sit back, relax and be taken literally from your front door to the terminal entrance in style and comfort.</p>
                
                <p>Whatever your holiday choice may be, let White 7 make it your perfect holiday Experience.</p>
                
              <a href="contact-us.php" class="engBtn">Enquire</a>  
                </div>

                <div class="TabbedPanelsContent"> <p>Whether you prefer modern shopping centres and outlet stores or quant cobbled streets that are home to historic and medieval buildings, you are literally spoilt for choice. From the city centres of Birmingham with the Bull Ring and the Mailbox, Manchester with Salford Quays and the Trafford Centre to the charming old cities of Chester, Oxford and Cambridge. Last but not least there is of course London with just about every shopping opportunity imaginable and after the exhausting retail ‘experience’, let White 7 take you and your shopping home in comfort and style.</p>
                <p>Whatever your shopping choice may be, let White 7 help make it your perfect shopping Experience.</p>
              <a href="contact-us.php" class="engBtn">Enquire</a>  
                </div>
                <div class="TabbedPanelsContent"> <p>Life moves at an ever increasing speed driven by the need to achieve the almost impossible. Relaxation is something we always promise ourselves but it never seems to happen. Why not immerse yourself in an array of treatments from experienced therapists which could include, thermal spa suites, hydrotherapy pools or that soothing tranquil massage. Or of course if you are feeling particularly energetic, there is always the swimming pool or multi – station gymnasium. The relaxation can continue on the way home with a  White 7 chauffeured journey.</p>
                <p>Whatever your spa choice may be, let White 7 help make it your perfect spa Experience.</p>
                
               <a href="contact-us.php" class="engBtn">Enquire</a> 
                </div>
                <div class="TabbedPanelsContent"> 
                
<p>Whether your palate is bias towards gourmet or casual dining, the choice is almost unlimited from the Michelin Star restaurant to the local ‘ pub.’ Whilst as a  rural county Shropshire provides a number of excellent dining establishments particularly so in Ludlow, there is of course more choice in larger towns or cities such as London, Birmingham, Manchester and Oxford. Naturally fine dining comes with fine wine which is an excellent reason to be chauffeured to your favourite venue.</p>

<p>Whatever your dining choice may be, let White 7 help make it your perfect dining Experience.</p>


<a href="contact-us.php" class="engBtn">Enquire</a>

                </div>
                <div class="TabbedPanelsContent"> <p>New York has Broadway but London has the West End and the Opera House with International Productions such as Miss Saigon, Jersey Boys, Mamma Mia, Swan Lake and Don Quixote or visit the 02 Arena and The Royal Albert Hall to see your favourite musical solo artist or band. Of course outside London for those  wanting a bias towards literature, The Royal Shakespeare Theatre at Stratford- Upon-Avon provides a diversity of plays to include of course, Shakespeare.</p>
                
                <p>Whatever your theatre  choice may be, let White 7 help make it your perfect theatre Experience.</p>
                
              <a href="contact-us.php" class="engBtn">Enquire</a>  
                </div>
        

        </div>
        
        <div class="cl"></div>
      </div>
</div>

<div class="col-lg-4">
<div class="expimg">
<img src="images/exp1.jpg" />
<img src="images/exp2.jpg" />
<img src="images/exp3.jpg" />
<img src="images/exp4.jpg" />
<img src="images/exp5.jpg" />
<img src="images/exp6.jpg" />

<div class="donwload"><div class="downloadbox-exp">
 
 <div> 
 <div class="downoption"><a href="http://www.white7.co.uk/wp-content/uploads/2014/09/White7_Experiences.pdf" target="_blank">Download
<span> Experiences Brochure</span></a></div></div>
 <div class="cl"></div>
 </div></div>
</div>
</div>



      
      
      
      
      
      
<div class="cl"></div>



<div class="cl"></div>

</div>

<?php include("footer.php"); ?>




</div>

</div>


<script src="responsive-tab/jquery.1.10.2.js" type="text/javascript"></script>

<link type="text/css" rel="stylesheet" href="responsive-tab/easy-responsive-tabs.css" />
<script src="responsive-tab/easyResponsiveTabs.js" type="text/javascript"></script>

<script type="text/javascript">
    $(document).ready(function () {
        $('#horizontalTab').easyResponsiveTabs({
            type: 'default', //Types: default, vertical, accordion           
            width: 'auto', //auto or any width like 600px
            fit: true,   // 100% fit in a container
            closed: 'accordion', // Start closed if in accordion view
            activate: function(event) { // Callback function if tab is switched
                var $tab = $(this);
                var $info = $('#tabInfo');
                var $name = $('span', $info);

                $name.text($tab.text());

                $info.show();
            }
        });

        $('#verticalTab').easyResponsiveTabs({
            type: 'vertical',
            width: 'auto',
            fit: true
        });
    });
</script>


<script>
$(document).ready(function() {

	$('#menu-toggle').click(function () {
      $('#menu').toggleClass('open');
      e.preventDefault();
    });
    
});
</script>

</body>
</html>
